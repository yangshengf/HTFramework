//
//  HTFramework.h
//  HTFramework
//
//  Created by htwater on 1/8/16.
//  Copyright © 2016 htwater. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HTFramework.
FOUNDATION_EXPORT double HTFrameworkVersionNumber;

//! Project version string for HTFramework.
FOUNDATION_EXPORT const unsigned char HTFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HTFramework/PublicHeader.h>

///网络请求
#import "RestEngine.h"

#import "MKNetworkRequest.h"
#import "MKNetworkHost.h"
#import "NSString+MKNKAdditions.h"

#import "MKCache.h"
#import "MKNetworkKit.h"
#import "MKObject.h"
#import "NSDate+RFC1123.h"
#import "NSDictionary+MKNKAdditions.h"
#import "NSHTTPURLResponse+MKNKAdditions.h"
#import "NSMutableDictionary+MKNKAdditions.h"
#import "UIAlertView+MKNKAdditions.h"
#import "UIImageView+MKNKAdditions.h"

///webview
#import "KINWebBrowserViewController.h"

#import "TUSafariActivity.h"
#import "ARChromeActivity.h"

///ViewDeck
#import "ViewDeck.h"
#import "IIViewDeckController.h"
#import "IISideController.h"
#import "IIWrapController.h"

///some Macros
#import "Macros.h"