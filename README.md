### 1. 网络请求示例

```
    self.NETEngine=[[RestEngine alloc] init];
    self.NETEngine.HOSTName=NetRestHost;> 服务器地址
    
    NSDictionary *params=[[NSDictionary alloc] init];
    [self.NETEngine SendRequestServiceWith:@"HTManageAppServer/GetAppList!COM"
                                    params:params
                                httpMethod:@"POST"
                               onSucceeded:^(NSDictionary *dic)
     {
         
         NSLog(@"%@",dic);
         
     } onError:^(NSError *error) {
         NSLog(@"%@",error);
     }];

```

### 2. WebView组件

- 导航方式（需要先加载UInavgationController组件）

```
KINWebBrowserViewController *webBrowser = [KINWebBrowserViewController webBrowser];
[webBrowser setDelegate:self];
    
[self.navigationController pushViewController:webBrowser animated:YES];
[webBrowser loadURLString:@"https://www.baidu.com/"];

```

- push方式

```
UINavigationController *webBrowserNavigationController = [KINWebBrowserViewController navigationControllerWithWebBrowser];
KINWebBrowserViewController *webBrowser = [webBrowserNavigationController rootWebBrowser];
[webBrowser setDelegate:self];
webBrowser.showsURLInNavigationBar = YES;
webBrowser.tintColor = [UIColor whiteColor];
webBrowser.barTintColor = [UIColor colorWithRed:102.0f/255.0f green:204.0f/255.0f blue:51.0f/255.0f alpha:1.0f];
webBrowser.showsPageTitleInNavigationBar = YES;
webBrowser.showsURLInNavigationBar = YES;
[self presentViewController:webBrowserNavigationController animated:YES completion:nil];
    
[webBrowser loadURLString:@"https://www.baidu.com/"];

```
> pragma mark - KINWebBrowserDelegate Protocol Implementation

```

(void)webBrowser:(KINWebBrowserViewController *)webBrowser didStartLoadingURL:(NSURL *)URL {
    NSLog(@"Started Loading URL : %@", URL);
}

(void)webBrowser:(KINWebBrowserViewController *)webBrowser didFinishLoadingURL:(NSURL *)URL {
    NSLog(@"Finished Loading URL : %@", URL);
}

(void)webBrowser:(KINWebBrowserViewController *)webBrowser didFailToLoadURL:(NSURL *)URL withError:(NSError *)error {
    NSLog(@"Failed To Load URL : %@ With Error: %@", URL, error);
}

```

### 3. 侧滑窗口
[详细使用说明，点击查看](https://github.com/yangshengfei/ViewDeck)

### 4. 公共宏定义
[详细使用说明，点击查看](https://github.com/yangshengfei/TBMacros)