//
//  RestEngine.h
//  AllDemo
//
//  Created by htwater-yangshengfei on 1/5/16.
//  Copyright © 2016 htwater. All rights reserved.
//

#import "MKNetworkHost.h"

@interface RestEngine : MKNetworkHost

typedef void (^DictionaryBlock)(NSDictionary * dic);
typedef void (^ErrorBlock)(NSError * error);

@property (strong,nonatomic) NSString *HOSTName;

//默认的要传参数的GET/POST请求
-(id) SendRequestServiceWith:(NSString * ) path
                      params:(NSDictionary *) params
                  httpMethod:(NSString*) method
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock;

//自定义hostName的要传参数的GET/POST请求
-(id) SendRequestServiceWith:(NSString *) hostName
                        path:(NSString * ) path
                      params:(NSDictionary *) params
                  httpMethod:(NSString*) method
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock;

//无需传参数的GET请求
-(id) SendGetRequestServiceWith:(NSString * ) path
                    onSucceeded:(DictionaryBlock) respondBlock
                        onError:(ErrorBlock) errorBlock;

//自定义hostName的无需传参数的GET请求
-(id) SendGetRequestServiceWith:(NSString * ) hostName
                           path:(NSString * ) path
                    onSucceeded:(DictionaryBlock) respondBlock
                        onError:(ErrorBlock) errorBlock;

//URL 请求
-(id) SendRequestServiceWith:(NSString *) url
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock;

//GET/POST请求
-(id) SendRequestServiceWith:(NSString*) path
                      params:(NSDictionary*) params
                  httpMethod:(NSString*) method
                        body:(NSData*) bodyData
                         ssl:(BOOL) useSSL
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock;

//自定义hostName的GET/POST请求
-(id) SendRequestServiceWith:(NSString*) hostName
                        path:(NSString * ) path
                      params:(NSDictionary*) params
                  httpMethod:(NSString*) method
                        body:(NSData*) bodyData
                         ssl:(BOOL) useSSL
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock;

@end
