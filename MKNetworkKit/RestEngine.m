//
//  RestEngine.m
//  AllDemo
//
//  Created by htwater-yangshengfei on 1/5/16.
//  Copyright © 2016 htwater. All rights reserved.
//

#import "RestEngine.h"
#import "MKNetworkKit.h"

@implementation RestEngine

-(id) SendRequestServiceWith:(NSString * ) path
                      params:(NSDictionary *) params
                     httpMethod:(NSString*) method
                    onSucceeded:(DictionaryBlock) respondBlock
                        onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:self.HOSTName];
    MKNetworkRequest *request=[engine requestWithPath: path params: params httpMethod:method];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

-(id) SendRequestServiceWith:(NSString *) hostName
                        path:(NSString * ) path
                      params:(NSDictionary *) params
                  httpMethod:(NSString*) method
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:hostName];
    MKNetworkRequest *request=[engine requestWithPath: path params: params httpMethod:method];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

-(id) SendGetRequestServiceWith:(NSString * ) path
                    onSucceeded:(DictionaryBlock) respondBlock
                        onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:self.HOSTName];
    MKNetworkRequest *request=[engine requestWithPath: path];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

-(id) SendGetRequestServiceWith:(NSString * ) hostName
                           path:(NSString * ) path
                    onSucceeded:(DictionaryBlock) respondBlock
                        onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:hostName];
    MKNetworkRequest *request=[engine requestWithPath: path];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

-(id) SendRequestServiceWith:(NSString *) url
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] init];
    MKNetworkRequest *request=[engine requestWithURLString: url];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;

}

-(id) SendRequestServiceWith:(NSString*) path
                      params:(NSDictionary*) params
                  httpMethod:(NSString*) method
                        body:(NSData*) bodyData
                         ssl:(BOOL) useSSL
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:self.HOSTName];
    MKNetworkRequest *request=[engine requestWithPath:path params:params httpMethod:method body:bodyData ssl:useSSL];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

-(id) SendRequestServiceWith:(NSString*) hostName
                        path:(NSString * ) path
                      params:(NSDictionary*) params
                  httpMethod:(NSString*) method
                        body:(NSData*) bodyData
                         ssl:(BOOL) useSSL
                 onSucceeded:(DictionaryBlock) respondBlock
                     onError:(ErrorBlock) errorBlock{
    
    MKNetworkHost *engine=[[MKNetworkHost alloc] initWithHostName:hostName];
    MKNetworkRequest *request=[engine requestWithPath:path params:params httpMethod:method body:bodyData ssl:useSSL];
    
    [request addCompletionHandler:^(MKNetworkRequest *completedRequest) {
        if(completedRequest.state==MKNKRequestStateCompleted){//succeeded
            
            NSDictionary *respondData=[NSDictionary dictionaryWithObjects:@[@"1",completedRequest.responseAsJSON] forKeys:@[@"State",@"Json"]];
            
            respondBlock(respondData);
            
        }else if (completedRequest.state==MKNKRequestStateError){//error
            errorBlock(completedRequest.error);
        }
        
    }];
    
    [engine startRequest:request];
    
    return request;
    
}

@end
